import React    from 'react';
import ReactDOM from 'react-dom';
import Data     from './data';
import {
  Row, Col, Button
}               from 'react-bootstrap';

// require('./routers/index.js');

var address = document.querySelector("#address");

class MainComponent extends React.Component{

  constructor(props){
    super(props);
    this.state = {
       data: [{
         first: "OOOOOOOOOOK",
         second: "The React example success !",
         third: "Third element in index one of the array ''data''"
       }, {
         first: "This is second index",
         second: "This is second element in this index"
       }]
      };
  }

  setData(){
    this.setState({
      data: [{
         first: "OK setState success",
         second: "The React example with setState success !",
         third: "Third element in index ''one'' of the array ''data'' is changed"
       }, {
         first: "This is second index ... veryyy small change",
         second: "This is second element in this index after setState"
       }]
    })
  }

  render(){
    return(
      <div>
        SUCCESS (or) Super!<br /><br />
        <Row>
        {  
          this.state.data.map(function (d, i) {
            return <Col key={i} xs={12}><Data data={d} /></Col>
          })
        }
        </Row>
        <br /> <br />
        <Button bsStyle={"success"} onClick={this.setData.bind(this)}>Click here (for setState example) ...</Button><br /><br />
        <a href={"router.html"}>React-router example</a>
      </div>
    )
  }

}


var div = document.getElementById('app');

ReactDOM.render(<MainComponent />, div);