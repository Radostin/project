import React    from 'react';
import ReactDOM from 'react-dom';
import {
  Panel, Button
}               from 'react-bootstrap';


export default class DataComponent extends React.Component {
  render(){
    return(
      <Panel>
        {this.props.data.first}<br />
        {this.props.data.second}<br />
        {this.props.data.third}
      </Panel>
    );
  }
}